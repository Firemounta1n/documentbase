package com.search;

import java.io.PrintWriter;
import java.io.IOException;
import java.sql.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Search
 */
@WebServlet("/Search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        Connection conn = null;
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "filebase?&relaxAutoCommit=true&useUnicode=true&characterEncoding=utf8";
        String driver = "com.mysql.jdbc.Driver";
        String userName = "filebase";
        String password = "filebase";
        
        Statement st;
        
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + dbName, userName, password);

            String search = request.getParameter("search");
            String query = "SELECT * FROM info WHERE tema RLIKE '" + search + "' OR nazvanie RLIKE '" + search + "' OR avtor RLIKE '" + search + "' OR god RLIKE '" + search + "' OR udc RLIKE '" + search + "' ";

            st = conn.createStatement();
            ResultSet rs = st.executeQuery(query);
            
            out.println("<HTML>");
            out.println("<HEAD>");
            out.println("<meta http-equiv=Content-Type content=text/html; charset=UTF-8>");
            out.println("<title>Result search</title>");
            out.println("<link href=css/table.css rel=stylesheet type=text/css>");
            out.println("</HEAD>");
            out.println("<center>");
            out.println("<h1 style='font-family: Arial, Helvetica, sans-serif; color: #666; text-shadow: 1px 1px 0px #fff;'>Информационно-справочная система</h1>");
            out.println("<strong style='font-family: Arial, Helvetica, sans-serif; color: #666; text-shadow: 1px 1px 0px #fff;'>Поиск</strong>");
            out.println("</center>");
            out.println("<p><a href=search.jsp><img src=pics/left.png width=30 height=30 border=0 alt=Назад></a></p>");
            out.println("<center>");
            out.println("<FORM id=form name=base>");
            out.println("<TABLE class=simple-little-table cellspacing=0>");
            out.println("<TR><th>Тема</th><th>Название</th><th>Автор</th><th>Год</th><th>УДК</th></TR>");
            while (rs.next()) {

                out.println("<TR>");
                out.println("<td>" + rs.getString(5) + "</td>");
                out.println("<td>" + rs.getString(6) + "</td>");
                out.println("<td>" + rs.getString(7) + "</td>");
                out.println("<td>" + rs.getString(8) + "</td>");
                out.println("<td>" + rs.getString(9) + "</td>");
                out.println("</TR>");

            }
            out.println("<TABLE/>");
            out.println("</FORM>");
            out.println("</CENTER>");
            out.println("</BODY>");
            out.println("</HTML>");
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
	}

}
