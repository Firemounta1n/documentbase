package upload.md5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet("/Upload_MD5")
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
				 maxFileSize=1024*1024*10,      // 10MB
				 maxRequestSize=1024*1024*50)   // 50MB

public class Upload_MD5 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
				
		//Для вывода текста на страницу в браузере
		response.setContentType("text/html; charset=UTF-8");

        PrintWriter out = response.getWriter();
   
        //Директория, в которую сохранится файл
		File uploadFiles = new File(System.getProperty("jboss.server.data.dir"), "srv");
		//Создаем директорию, если ее не существует
		if (!uploadFiles.exists()) {   	 
            uploadFiles.mkdir();
        }
				
		try {
		//Блок для сбора данных о файле и его сохранение	
        Part filePart = request.getPart("file"); 
		String fileNameOld = filePart.getSubmittedFileName();
		final String fileName = new String (fileNameOld.getBytes("ISO-8859-1"));
		InputStream fileContent = filePart.getInputStream();
		
		//Создаем условие для пустого файла
		if(fileName == null || fileName.equals("")){
			System.out.println("File Name can't be null or empty");
			response.sendRedirect("http://localhost:8080/FileBase/");
		}
        
        //Создаем условие если файл уже есть
        File f = new File(uploadFiles + File.separator + fileName);
        System.out.println("File location on server::"+f.getAbsolutePath());
        if(f.exists() && !f.isDirectory()){
        	System.out.println("File exists");
        	out.println("<script type=\"text/javascript\">");
        	out.println("alert('Такой файл уже существует');");
        	out.println("location='index.jsp';");
        	out.println("</script>");
        return;
        }
        
        //Записываем файл в директорию
        filePart.write(uploadFiles + File.separator + fileName);
        
        	try {
            //Блок подсчета MD5
    			MessageDigest md = MessageDigest.getInstance("MD5");

    	        byte[] dataBytes = new byte[1024];
    	 
    	        int nread = 0; 
    	        while ((nread = fileContent.read(dataBytes)) != -1) {
    	          md.update(dataBytes, 0, nread);
    	        };
    	        
    	        byte[] mdbytes = md.digest();
    	 
    	        StringBuffer sb = new StringBuffer();
    	        for (int i = 0; i < mdbytes.length; i++) {
    	        	sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
    	        }
    			
    			try {
    			// Блок записи данных в БД
    				Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/filebase?&relaxAutoCommit=true&useUnicode=true&characterEncoding=utf8","filebase","filebase");
    				Statement stm = con.createStatement();
    				ResultSet rs = stm.executeQuery("SELECT * FROM states WHERE state LIKE '%"+request.getParameter("tags1")+"%'");
    				rs.next();
    				
    				String UDC = new String(rs.getString(3));
    				rs.close();
    				
    				String filePath = uploadFiles + File.separator + fileName;
    			    String MD5 = sb.toString();
    				PreparedStatement pst = (PreparedStatement) con.prepareStatement("insert into filebase.info (File_Name,File_Path,MD5,tema,nazvanie,avtor,god,udc) values (?,?,?,?,?,?,?,?);");
    				pst.setString(1, fileName);
    				pst.setString(2, filePath);
    				pst.setString(3, MD5);
    				pst.setString(4, request.getParameter("tags1"));
    				pst.setString(5, request.getParameter("name"));
    				pst.setString(6, request.getParameter("autor"));
    				pst.setString(7, request.getParameter("year"));
    				pst.setString(8, UDC);
    				
    				con.commit();
                
    				int i = pst.executeUpdate();
    				
    				if(i!=0) {  
    					response.sendRedirect("http://localhost:8080/FileBase/");
    				}  
    				
    				stm.close();
    				con.close();
  		      		pst.close();
  		      		
    			} catch (SQLException e) {
    				System.out.println("Can't write data in MySQL");
    				out.println(e);  
    			}
            
    		} catch (NoSuchAlgorithmException e) {
    			System.out.println("Have problem with creating MD5" + e);
    			e.printStackTrace();
    		}
            
		} catch (FileNotFoundException e) {
			out.write("Exception in uploading file.");
			out.println(e);
		}  
	}
}
