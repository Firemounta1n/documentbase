<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.sql.*" %>
<%@page import="java.util.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%	
		try {
			String s[]=null;
			Class.forName("com.mysql.jdbc.Driver");

			// Подключение к БД
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/filebase?&relaxAutoCommit=true", "filebase", "filebase");
			
			//Получение элементов из БД
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery("SELECT * FROM states");
			
			List<String> li = new ArrayList<String>();
			
			while (rs.next ()){
				li.add(rs.getString(2));
			}
			
			String[] str = new String[li.size()];
		    Iterator<String> it = li.iterator();
		 
		    int i = 0;
		    while(it.hasNext()){
		    	String p = (String)it.next();
		        str[i] = p;
		        i++;
		    }
				
		    String query = (String)request.getParameter("q");
		    int cnt=1;
		    for(int j=0;j<str.length;j++){
		    	if(str[j].toUpperCase().startsWith(query.toUpperCase())){
		        	out.print(str[j]+"\n");
		            if(cnt>=10)// 10 = Количество видимых вариантов в выпадающем списке (autocomplite)
		            break;
		            cnt++;
		        }
		    }
		    
			rs.close();
			stm.close();
			con.close();
		}catch(Exception e){
			System.out.println("error"+e);
		}
%>