<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.io.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Search Base</title>

	<script type="text/javascript" src="JS/jquery-1.4.2.min.js"></script>
	<script src="JS/jquery.autocomplete.js"></script>
  	<link rel="stylesheet" type="text/css" href="css/autocomplete.css" />
  	<link href="css/table.css" rel="stylesheet" type="text/css">
  	
<script type="text/javascript">
	 $(function() {
			$("#search").autocomplete("states.jsp");
	}); 
</script>
<style>
	h1, form, strong {
		font-family: Arial, Helvetica, sans-serif;
		color: #666;
		text-shadow: 1px 1px 0px #fff;
</style>
</head>
<body>
<center>		
	<h1>Информационно-справочная система</h1>
	<strong>Поиск</strong>
</center>
	<p><a href="index.jsp"><img src="pics/left.png" width="30" height="30" border="0" alt="Назад"></a></p>
	<center>
	<form id="form" name="frm" method="post" action="Search">
  			<input type="text" name="search" id="search" size="40px" value="">
  			<input type="submit" name="submit" value="Найти">	
	</form>
  </center>
 </body>
</html>