<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.io.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>File Base</title>
	
	<script type="text/javascript" src="JS/jquery-1.4.2.min.js"></script>
	<script src="JS/jquery.autocomplete.js"></script>
  	<link rel="stylesheet" type="text/css" href="css/autocomplete.css" />
  	<link href="css/table.css" rel="stylesheet" type="text/css">
  	
	<script type="text/javascript">
	function checkInput(){
		if (document.UDC.tags1.value == "")
			alert ( "Пожалуйста введите тему.");
		if (document.UDC.file.value == "")
			alert ( "Пожалуйста выберите файл.")
		return false;
		}
	
	 $(function() {
			$("#tags1").autocomplete("states.jsp");
	}); 
</script>
<style>
	h1, form, strong {
		font-family: Arial, Helvetica, sans-serif;
		color: #666;
		text-shadow: 1px 1px 0px #fff;
	}
	input {
		margin-left: 20px;
	}
	label {
		margin-left: 40px;
	}
	.year {
		margin-left: 40px;
	}
</style>
</head>
<body>
   	<center>		
	<h1>Информационно-справочная система</h1>
	<strong>Загрузка</strong>
	</center>
	<p><a href="search.jsp"><img src="pics/right.png" width="30" 
  height="30" border="0" alt="Вперед"></a></p>
	<center>
	<form id="form" name="UDC" enctype="multipart/form-data" method="post" action="Upload_MD5" onsubmit="checkInput();">
			<label for="tags1">Выберите тему для УДК:</label>
  			<input type="text" name="tags1" id="tags1" size="40px" value="">
  			
  			<label id="year" class="year" for="year"> Введите год издания:</label>
  			<input type="text" name="year" id="year" size="20px" value=""><br>
  			
  		<br><label for="name">Введите название:</label>
  			<input type="text" name="name" id="name" size="38px" value="">
			
			<label for="autor"> Введите автора:</label>
  			<input type="text" name="autor" id="autor" size="32px" value=""><br>
		
		<br>Выберите загружаемый файл: <input name="file" type="file" value="" size="65px"><br>				
  			<br><input type="submit" value="Загрузить">
			<input type="reset" value="Очистить">
	</form>
	<form id="form" name="base">
	<% Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/filebase?&relaxAutoCommit=true","filebase","filebase");
         Statement statement = connection.createStatement();
         ResultSet resultset = statement.executeQuery("select * from info");
    %>
		<table class="simple-little-table" cellspacing="0">
            <tr>
            	<th>File_id</th>
                <th>Имя файла</th>
                <th>Путь</th>
                <th>MD5sum</th>
                <th>Тема</th>
                <th>Название</th>
                <th>Автор</th>
                <th>Год</th>
                <th>УДК</th>
            </tr>
            <% while(resultset.next()){ %>
            <tr>
            	<td> <%= resultset.getString(1) %></td>
                <td> <%= resultset.getString(2) %></td>
                <td><a href="file://<%= resultset.getString(3) %>"><%= resultset.getString(3) %></a></td>
                <td> <%= resultset.getString(4) %></td>
                <td> <%= resultset.getString(5) %></td>
                <td> <%= resultset.getString(6) %></td>
                <td> <%= resultset.getString(7) %></td>
                <td> <%= resultset.getString(8) %></td>
                <td> <%= resultset.getString(9) %></td> 
            </tr>
            <% } %>
        </table>
      </form>
  </center>
 </body>
</html>





